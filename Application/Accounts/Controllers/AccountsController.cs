﻿using System.Net;
using System.Web.Mvc;

namespace RegistryHopRestService.Application.Accounts.Controllers
{
    using RegistryHopRestService.Infrastructure.Application.Infrastructure.Controllers;

    using RegistryHopRestService.BLL.Application.Accounts.Models;
    using RegistryHopRestService.BLL.Application.Accounts.Services;
    using RegistryHopRestService.BLL.Application.Accounts.Exceptions;

    [RoutePrefix("accounts")]
    public class AccountsController : CorsEnabledApiController
    {
        const string AccountCreatedMessage = "Account created.  Please check your email for the confirmation code.";

        IAccountCreateService accountCreateService;

        public AccountsController(AccountCreateService accountCreateService)
        {
            this.accountCreateService = accountCreateService;
        }

        [HttpPost]
        [ActionName("Index")]
        public ActionResult Create(AccountCreateRequest accountCreateRequest)
        {
            try
            {
                accountCreateService.Create(accountCreateRequest);
                return HttpResponse(HttpStatusCode.Created, new {
                    Message = AccountCreatedMessage
                });
            }
            catch (AccountCreateException e)
            {
                return HttpError(HttpStatusCode.BadRequest, e.Message);
            }
        }
    }
}