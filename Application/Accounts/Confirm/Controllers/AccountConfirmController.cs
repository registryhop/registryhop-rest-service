﻿using System.Net;
using System.Web.Mvc;

namespace RegistryHopRestService.Application.Accounts.Confirm.Controllers
{
    using RegistryHopRestService.Infrastructure.Application.Infrastructure.Controllers;
    using RegistryHopRestService.BLL.Application.Accounts.Confirm.Models;
    using RegistryHopRestService.BLL.Application.Accounts.Confirm.Services;

    [Route("accounts/confirm")]
    public class ConfirmController : CorsEnabledApiController
    {
        private IAccountConfirmService accountConfirmService;

        public ConfirmController(AccountConfirmService accountConfirmService)
        {
            this.accountConfirmService = accountConfirmService;
        }

        [HttpPost]
        public ActionResult Confirm(AccountConfirmRequest accountConfirmRequest)
        {
            return HttpResponse(HttpStatusCode.OK,
                accountConfirmService.Confirm(accountConfirmRequest));
        }
    }
}